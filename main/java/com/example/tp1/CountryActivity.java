package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Intent intent = getIntent();

        final String countryName = intent.getStringExtra("countryName");
        final Country country = CountryList.getCountry(countryName);

        final String image = country.getmImgFile();
        final Resources res = this.getResources();
        int resId = res.getIdentifier(image, "drawable", getPackageName());

        ImageView ivFlag = (ImageView) findViewById(R.id.flagView);
        ivFlag.setImageResource(resId);

        final TextView tvCountry = (TextView) findViewById(R.id.titleView);
        tvCountry.setText(countryName);

        final TextView tvCapital = (TextView) findViewById(R.id.capitalText);
        tvCapital.setText(country.getmCapital());

        final TextView tvLanguage = (TextView) findViewById(R.id.languageText);
        tvLanguage.setText(country.getmLanguage());

        final TextView tvCurrency = (TextView) findViewById(R.id.currencyText);
        tvCurrency.setText(country.getmCurrency());

        final TextView tvPopulation = (TextView) findViewById(R.id.populationText);
        tvPopulation.setText(Integer.toString(country.getmPopulation()));

        final TextView tvArea = (TextView) findViewById(R.id.areaText);
        tvArea.setText(Integer.toString(country.getmArea()));

        Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(tvCapital.getText().toString());
                country.setmLanguage(tvLanguage.getText().toString());
                country.setmCurrency(tvCurrency.getText().toString());
                country.setmPopulation(Integer.parseInt(tvPopulation.getText().toString()));
                country.setmArea(Integer.parseInt(tvArea.getText().toString()));
                finish();
            }
        });


    }
}
